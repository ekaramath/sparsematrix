# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
I have implemented a small library in C++ to represent numeric sparse matrices and vectors. Sparse matrices and vectors are quite large but have very few non zero elements (at each row or column). Thinking about it in terms of matrices only, a sparse nxn matrix would need around O(n^2) memory in a conventional implementation. However, by assuming sparsitiy and storing nonzero elements only, the space requirement is O(n). Operations such as matrix product could as well be O(n) (when the matrices are quite sparse). To see this reduction in complexity (from around O(n^3) to O(n)) remember that when you multiply one row of the lhs matrix by the other matrix, all you need to know is a subset of rhs matrix's columns which could potentially lead to nonzero elements. Assuming the matrices are very sparse, this number is much smaller than 'n' and hence, can be approximated as O(1) (no proof, just an assumption verified by some experiments). 
* Version
This is the first version, i.e., 1.0.



### How do I get set up? ###

* Summary of set up
Clone and compile the code:
Linux: 
 git clone https://ekaramath@bitbucket.com/ekaramath/sparsematrix.git
Compile using latest version of g++:
g++ -o main.o sparse_vector.h sparse_matrix.h main.cpp

* Dependencies
C++11 STL.

* How to run tests
Refer to the example given (main.cpp).


### Who do I talk to? ###

* Repo owner or admin: Ehsan Karamad (ekaramad@gmail.com)

#ifndef __sparse_matrix_h__
#define __sparse_matrix_h__

#include "sparse_vector.h"

template<class T> sparse_matrix {
 public:
  sparse_matrix(int n_, int m_) {
    n = n_; m = m_;
    rows.resize(n);
    for (int i = 0; i < n; i++) {
      sparse_vector<T> new_row(m);
      rows.push_back(new_row);
    };
    
  };

  sparse_vector<T>& operator[] (int index) {
    return rows[index];
  };
  
  void operator+= (const sparse_matrix<T>& o) {
    if (o.dim()[0] != dim()[0] || o.dim()[1] != dim()[1]) return;
    for (int i = 0; i < n; i++) {
      this->rows[i] += o.rows[i];
    };
  };
  sparse_matrix<T>& operator+ (const sparse_matrix<T>& o) const {
    sparse_matrix<T> res = *this;
    res += o;
    return res;
  };
  // assignment operator
  sparse_matrix<T>& operator= (const sparse_matrix<T>& o) {
    if (this != &o) {
      this->n = o.n; this->m = o.m;
      this->rows.resize(n);
      for (int i = 0; i < n; i++) {
	this->rows[n] = o.rows[n];
      };
    };
    return *this;
  };
  sparse_vector<T>& operator (const sparse_vector<T>& o) const {
    return (sparse_vector<T>& ) 0;
  };

 public:
  int[] dim() {
    int d[2] = {n, m};
    return d;
  };
  // remove zero entries, and keep a short list of column information.
  void optimize() {
    if (optimized) return;
    non_zero_rows_at.resize(m);
    for (int i = 0; i < n; i++) {
      for (typename map<int, T>::iterator it = row.iterator().begin(); it != row.iterator().end(); ++it) {
	if (it->second == (T) 0) row[it->first] = (T) 0;
	non_zero_rows_at[it->first].add(i);
      };
    };
    optimized = true;
  };
 private:
  vector<sparse_vecctor<T>> rows;
  vector<set<int>> non_zero_rows_at;
  int n, m;
  bool optimized = false;
};
#endif

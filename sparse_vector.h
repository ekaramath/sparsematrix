#ifndef __sparse_vector_h__
#define __sparse_vector_h__

#include <map>
using namespace std;

template<class T> class sparse_vector {  
 public:
 sparse_vector(int length_) : length(length_) {
  };
 sparse_vector(const sparse_vector<T>& other) : length(other.length) {
    for (typename map<int, T>::iterator  it = other.elements.begin(); it != other.elements.end(); ++it) {
      if (it->second == (T) 0) continue; // do not copy over null entries.
      elements[it->first] = it->second;
    };      
  };
 public:
  T& operator [] (int index) {
    if (elements.count(index) == 0) return (T&) 0;
    return elements[index];
  };

  sparse_vector<T>& operator= (const sparse_vector<T>& o) {
    if (this != &o) {
      this.length = o.length;
      for (typename map<int, T>::iterator it = o.elements.begin(); it != o.elements.end(); ++it) {
	if (it->second == (T) 0) continue;
	elements[it->first] = it->second;
      };
    };
    return *this;
  };
  // add two sparse vectors.
  sparse_vector<T>& operator+ (const sparse_vector<T>& o) const {
    sparse_vector<T> res = *this;
    res += o;
    return res;
  };
  // add self with another sparse vector.
  void  operator+= (const sparse_vector<T>& o) {
    for (typename map<int, T>::iterator it = o.elements.begin(); it != o.elements.end(); ++it) {
      if (it->second == (T) 0) continue;
      if (elements.count(it->first) == 0) elements[it->first] = it->second;
      else elements[it->first] = elements[it->first] + it->second;
    };
  };

  void operator *= (const T& o) {
      for (typename map<int, T>::iterator it = elements.begin(); it != elements.end(); it++) {
	if (it->second == (T) 0) elements.erase(it);
	else it->second = it->second * o;
      };
  };

  sparse_vector<T>& operator * (const T& o) const {
    sparse_vector<T>& res = *this;
    res *= o;
    return res;
  };
  // inner product of two sparse vectors.
  T& operator * (const sparse_vector<T>& o) const {
    T res = (T) 0;
    for (typename map<int, T>::iterator it = elements.begin(); it != elements.end(); ++it) {
      if (o.elements.count(it->first) == 1) {
	res = res + it->second * o.elements[it->first];
      };
    };
    return res;
  };
  
 private:
  map<int, T> elements;
  int length;
  };






#endif

